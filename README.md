# Nabunote Reloaded

A Rich Text Editor using ReactJS, medium-editor and emoji-mart on the Front End. Bundled with Parcel

## Building and running on localhost

First install dependencies:

```sh
npm install
```

To run in hot module reloading mode:

```sh
npm start
```

To create a production build:

```sh
npm run build-prod
```

## Running

Open the file `dist/index.html` in your browser

## Credits

Made with [createapp.dev](https://createapp.dev/)
